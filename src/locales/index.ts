import {el} from './el';
import {en} from './en';

export const LOCALES = {
    el,
    en
};
